output "vm_name" {
    value = module.terraform_pve.vm_name
}

output "vm_ip" {
  value = module.terraform_pve.ip_address
}