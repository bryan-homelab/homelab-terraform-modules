variable "pve_password" {
  type = string
  description = "Password for your proxmox user"
}

variable "pve_username" {
  type = string
  description = "Username and realm to log in with to proxmox "
  default = "root@pam"
}

variable "pihole_api_token" {
  type = string
  description = "API token for your pihole instance"
}

variable "pve_url" {
  type = string
  description = "URL to the api of your promxox instance"
  default  = "https://pve-01.homelab.com:8006/api2/json"
}

variable "pihole_url" {
  type = string
  description = "URL to your pihole instance"
  default = "http://pi.hole/"
}

terraform {
  required_providers {
    proxmox = {
      source = "telmate/proxmox"
      version = "2.9"
    }
    pihole = {
      source = "ryanwholey/pihole"
    }
  }
}


provider "proxmox" {
    pm_tls_insecure = true
    pm_api_url = var.pve_url
    pm_password = var.pve_password
    pm_user = "root@pam"
}

provider "pihole" {
  url = var.pihole_url 

  api_token = var.pihole_api_token # PIHOLE_API_TOKEN
}

module "terraform_pve" {
    source = "../"
    vm_name = "testvm"
    vm_ip = "10.10.10.35"
    vm_gateway = "10.10.10.1"
}