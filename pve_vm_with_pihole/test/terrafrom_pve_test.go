package test

import (
	"net"
	"os"
	"testing"
	"fmt"

	"github.com/gruntwork-io/terratest/modules/terraform"
	"github.com/stretchr/testify/assert"
)

func TestPVEVMWithPiHole(t *testing.T) {
	// retryable errors in terraform testing.
	terraformOptions := terraform.WithDefaultRetryableErrors(t, &terraform.Options{
		TerraformDir: ".",
		Vars: map[string]interface{}{
			"pve_username":     "root@pam",
			"pve_password":     os.Getenv("PVE_PASS"),
			"pihole_api_token": os.Getenv("PIHOLE_APIKEY"),
		},
	})

	defer terraform.Destroy(t, terraformOptions)

	terraform.InitAndApply(t, terraformOptions)

	var vm_test_name = "testvm.homelab.com"
	var vm_test_ip = "10.10.10.35"

	ips, err := net.LookupIP(vm_test_name)

	if err != nil {
		fmt.Fprintf(os.Stderr, "Could not get IPs: %v\n", err)
	}

	assert.Equal(t, vm_test_ip, ips[0].String())

}
