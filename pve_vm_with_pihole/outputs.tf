output "vm_name" {
    value = proxmox_vm_qemu.terraform_pve.name
}

output "memory" {
    value = proxmox_vm_qemu.terraform_pve.memory
}

output "ip_address" {
    value = proxmox_vm_qemu.terraform_pve.default_ipv4_address
}