resource "proxmox_vm_qemu" "terraform_pve" {

    name = var.vm_name
    desc = var.vm_desc

    # Node name has to be the same name as within the cluster
    # this might not include the FQDN
    target_node = var.node

    # The destination resource pool for the new VM
    # pool = "PVE"

    # The template name to clone this vm from
    clone = "ubuntu-cloudinit-23-04"

    # Activate QEMU agent for this VM
    agent = 1
    # enabling_logging = true
    os_type = "cloud-init"
    cores = var.cpu
    sockets = 1
    vcpus = 0
    cpu = "host"
    memory = var.memory
    scsihw = "lsi"

    # Setup the disk
    disk {
        size = "60G"
        type = "virtio"
        storage = "TruenasNVME"
    }

    sshkeys = var.ssh_key

    network {
        model = "virtio"
        bridge = "vmbr0"
    }

    nameserver = "10.10.10.56"
    searchdomain = "homelab.com"

    ciuser = "breid01"

    # Setup the ip address using cloud-init.
    # Keep in mind to use the CIDR notation for the ip.
    ipconfig0 = "ip=${var.vm_ip}/${var.vm_netmask},gw=${var.vm_gateway}"

  # provisioner "remote-exec" {
  #   inline = var.commands == null ? ["echo 'Holy shit it worked!"] : var.commands

  #   connection {
  #       type     = "ssh"
  #       user     = "breid01"
  #       private_key = file("~/.ssh/id_rsa")
  #       host     = "${var.vm_name}.${var.vm_domain}"
  #   }
  # }
}

resource "pihole_dns_record" "record" {
  domain = "${var.vm_name}.${var.vm_domain}"
  ip     = var.vm_ip
}