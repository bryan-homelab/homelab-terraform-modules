variable "vm_name" {
  type = string
    description = "Name of your VM to create in proxmox. This will also form the start of your DNS record"
}

variable "vm_domain" {
  type = string
  description = "Domain of your network, for creating DNS records in pihole"
  default = "homelab.com"
}

variable "vm_desc" {
  type = string
    default = "VM built by Terraform"
}

variable "cpu" {
  type = number
  default = 1
}

variable "memory" {
  type = number
    default = 1024
}

variable "vm_ip" {
  type = string
    description = "IP address in CIDR format for the vm. eg 10.10.10.20/24"
}

variable "vm_netmask" {
  type = string
  description = "Netmask of subnet, eg 24"
  default="24"
}

variable "vm_gateway" {
  type = string
  description = "Gateway to use for the VM"
}

variable "ssh_key" {
  # type = string
  default = <<EOF
  ssh-rsa AAAAB3NzaC1yc2EAAAADAQABAAABgQDIjHTts9xQtsLfiPHA8Eh+Uov8d/lXAUZW4ZoE0B+Fravw7jqz1Oxa/0eLnR0akufIbR8jv5lBA/Pdq7EffJeqOKcQas94kq1lYwF9dSwGexgQbKVBH8AYkE72i7hEyLK9cE+eCUHYxZpbxb7As3RdZ184kQV9b+0HCj5yKaS++DE2DbEOWOqcSbZm8d94Smr8W5lzfd8RDuk1dZs4yGd4KkITwH+I8ppRoFcxp5SPXXTqbBNbFgs1jdqj7B+5+WGi6bqeC32byX0vn1qvLBl1gmnOGZS/a8c93fBOEvGyqrBAUbU351fg7lCKRQBB9Ms4Kvc4G/pU4p155DTzGMdc4tNwRXJvm/8Y3h+JREe/LtzH2CY3bewPwVtbFerkBXanxSRqf8MfOO77620vUflNhmKKJapJwzbmSL03rRBNOWlX0iI9bPeljcZQjmM8gOpVSOiXcG2/0TODoPlvYRbnIBHG84vTvEauFCLXFZlirb+J4OF1P0LoL/sAnPilu5E= weere@DESKTOP-SJD5K8N
  EOF
}

variable "commands" {
  type = list(string)
  default = null
}

variable "node" {
  description = "Node to deploy the vm to."
  default = "pve-01"
  type = string
}